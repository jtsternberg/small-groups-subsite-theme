<?php include ('sg-blog-header.php'); ?>
  <div id="content">
  	<div id="leftcolumn">
  		<?php if ( is_front_page() ) : ?>
			<h1 class="mainpage"><?php bloginfo('name'); ?></h1>
  		<?php elseif ( is_category() ) : ?>
  			<h1>"<?php single_cat_title(); ?>" Category Archives</h1>
  		<?php elseif ( is_archive() ) : ?>
  			<h1><?php bloginfo('name'); ?> Archives</h1>
  		<?php elseif ( is_search() ) : ?>
  			<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentyten' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
		<?php endif; ?>

	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<hr />
				<p><?php the_time('F jS, Y') ?> by <a href="<?php echo get_site_url(1); ?>/staff/<?php the_author_meta('staff'); ?>"><?php the_author_firstname(); ?> <?php the_author_lastname(); ?></a></p>

				<div class="entry">
					<?php the_post_thumbnail( 'cr-featured' ); ?>
					<?php if ( is_search() ) : ?>
					<p><?php the_excerpt(); ?></p><br />
					<?php else : ?>
					<?php the_content('Read the rest of this entry &raquo;'); ?>
					<?php endif; ?>
				</div>
				<?php if( function_exists('ADDTOANY_SHARE_SAVE_KIT') ) { ADDTOANY_SHARE_SAVE_KIT(); } ?>

				<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &rarr;', '1 Comment &rarr;', '% Comments &rarr;'); ?></p>

			</div>
				<hr class="bottomhr" />
		<?php endwhile; ?>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&larr; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &rarr;') ?></div>
		</div>

	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php get_search_form(); ?>

	<?php endif; ?>
</div>
<?php
include('sidebar.php');
switch_to_blog(1);
include (TEMPLATEPATH . '/footers/SG-footer.php');
restore_current_blog();
