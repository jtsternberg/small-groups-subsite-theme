<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<title><?php wp_title('&larr;', true, 'right'); ?><?php bloginfo('name'); ?></title>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

<?php wp_head(); ?>


</head>
<body>

<div id="container">
  	<div id="header">

  	  <div id="logo">
		    <h1 class="logotext"><a href="<?php echo get_site_url(1); ?>">Generations Church</a></h1>
  	  </div>

  	  <div id="topNav">
        <ul class="topMid">
          <?php switch_to_blog(1);
          wp_nav_menu( array( 'theme_location' => 'top' ) );
          restore_current_blog();
          ?>
        </ul>
        <div class="searchbar">
          <div id="sitemap"><a href="<?php get_bloginfo( wpurl ); ?>/site-map">site map</a></div>
            <?php get_template_part( 'repeat_elements/searchform' ); ?>
        </div>
    </div>
  </div>


  	 <div id="headerimg" class="SGheaderimg">
 	 	<div id="navbox">
 	  		<div id="navright">
     		 <ul class="navlinks2">
      		 <li><a href="http://generationschurch.com/weekend-worship"><img src="<?php echo get_template_directory_uri(); ?>/images/W.png" alt="Weekend Worship" class="floatLeft" /></a></li>
      		 <li><a href="http://generationschurch.com/family-ministry"><img src="<?php echo get_template_directory_uri(); ?>/images/FM.png" alt="Family Ministries" class="floatLeft" /></a></li>
      		 <li><a href="http://generationschurch.com/missions-outreach"><img src="<?php echo get_template_directory_uri(); ?>/images/M.png" alt="Missions &amp; Outreach" class="floatLeft" /></a></li>

      		 </ul>
 			</div>

 			<div id="navleft">
 			 <a href="http://generationschurch.com/small-groups"><img src="<?php echo get_template_directory_uri(); ?>/images/SG.png" alt="Small Groups" class="navleft" />Small Groups</a>
 			 </div>

 			<div id="navcenter">
 			 <div id="navleft2links">
 			  <ul class="navlinks3">
 	   		  <?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'SG', 'fallback_cb' => array( 'GC_SG_Child', 'fallback_menu' ) ) ); ?>
 			  </ul>
 			 </div>
 			</div>

 	 	</div>
  	 </div>
