<?php include ('sg-blog-header.php'); ?>

  <div id="content">
  	<div id="leftcolumn">

	<?php if (have_posts()) : ?>

		<?php while (have_posts()) : the_post(); ?>

			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h1><?php the_title(); ?></h1>
				<?php if ( ! is_page() ) : ?>
				<p><?php the_time('F jS, Y') ?> by <a href="<?php echo get_site_url(1); ?>/staff/<?php the_author_meta('staff'); ?>"><?php the_author_firstname(); ?> <?php the_author_lastname(); ?></a></p>
				<?php endif; ?>


				<div class="entry">
					<?php the_post_thumbnail( 'cr-featured' ); ?>
					<?php the_content('Read the rest of this entry &raquo;'); ?>
				</div><?php if( function_exists('ADDTOANY_SHARE_SAVE_KIT') ) { ADDTOANY_SHARE_SAVE_KIT(); } ?>

				<?php if ( ! is_page() ) : ?>
				<?php comments_template(); ?>

				<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &rarr;', '1 Comment &rarr;', '% Comments &rarr;'); ?></p>
				<?php endif; ?>

			</div>

		<?php endwhile; ?>

		<?php if ( ! is_page() ) : ?>
		<div class="navigation">
			<div class="alignright"><?php next_post_link('%link', 'Newer Post &rarr;'); ?></div>
			<div class="alignleft"><?php previous_post_link('%link', '&larr; Older Post'); ?></div>

		</div>
		<?php endif; ?>

	<?php else : ?>

		<h2 class="center">Not Found</h2>
		<p class="center">Sorry, but you are looking for something that isn't here.</p>
		<?php get_search_form(); ?>

	<?php endif; ?>


</div>
<?php include('sidebar.php');?>
  	<?php
switch_to_blog(1);
include (TEMPLATEPATH . '/footers/SG-footer.php');
restore_current_blog();
