<?php
class GC_SG_Child {

	// A single instance of this class.
	public static $instance = null;

	/**
	 * Creates or returns an instance of this class.
	 * @since  0.1.0
	 * @return GC_SG_Child A single instance of this class.
	 */
	public static function go() {
		if ( self::$instance === null )
			self::$instance = new self();

		return self::$instance;
	}

	public function __construct() {
		add_action( 'init', array( $this, 'init' ) );

	}

	function init() {
		register_sidebar( array(
			'name'          =>'Small Group Blog',
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<h2>',
			'after_title'   => '</h2>',
			'id'            => 'crblog',
		) );

		unregister_sidebar('w');
		unregister_sidebar('fm');
		unregister_sidebar('sg');
		unregister_sidebar('cr');
		unregister_sidebar('sa');
		unregister_sidebar('sm');
		unregister_sidebar('gc');
		add_image_size( 'cr-featured', 700, 200, true );

	}

	public static function fallback_menu() {
		switch_to_blog(1);
		wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'SG' ) );
		restore_current_blog();
	}

}
GC_SG_Child::go();
