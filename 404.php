<?php include ('sg-blog-header.php'); ?>
<div id="content">
	<div id="leftcolumn">
		<?php if (have_posts()) : ?>

			<?php while (have_posts()) : the_post(); ?>

				<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
					<h1><?php the_title(); ?></h1>
					<div class="entry">
						<?php the_content('Read the rest of this entry &raquo;'); ?>
					</div>
				</div>

			<?php endwhile; ?>

			<div class="navigation">
				<div class="alignleft"><?php next_posts_link('&larr; Older Entries') ?></div>
				<div class="alignright"><?php previous_posts_link('Newer Entries &rarr;') ?></div>
			</div>

		<?php else : ?>

			<h2 class="center">Not Found</h2>
			<p class="center">Sorry, we don't seem to have what you're looking for, but feel free to try searching.</p>

			<div class="searchbar2"><?php get_template_part( 'repeat_elements/searchform' ); ?></div>

		<?php endif; ?>
	</div>
<?php
include('sidebar.php');
switch_to_blog(1);
include (TEMPLATEPATH . '/footers/SG-footer.php');
restore_current_blog();
